#!/usr/bin/perl

# Script to generate the website using Markdown.pm.  Redistributable
# under the same terms, but I wouldn't bother if I were you.

use strict;
use warnings;

use File::Find;
use File::Basename;
use File::Path 'make_path';
use Cwd;

# Add script dir to lib path so we can find Markdown.pm
use FindBin;
use lib "$FindBin::Bin/";

use Markdown 'markdown';



sub msg {
  print join(" ", @_), "\n";
}

sub slurp {
  my ($path) = @_;

  open my $fh, "<", $path
    or die "Unable to open '$path' for reading.\n";

  local $/;     # Remove delimiter so the next line reads everything.
  my $lines = <$fh>;

  return $lines;
}

sub unslurp {
  my ($path, $data) = @_;

  open my $fh, ">", $path
    or die "Unable to open $path for writing.\n";

  print $fh $data;
}


# Page title comes from the first hash-style Markdown header.
sub find_title {
  my ($text) = @_;

  for my $line (split(/\n/, $text)) {
    next unless $line =~ /^\s*#+/;
    $line =~ s/^\s*#+\s*//;
    $line =~ s/#*\s*$//;
    return $line;
  }

  return "";
}

sub find_rootpath {
  my ($path) = @_;

  my $count = scalar(File::Spec->splitdir($path)) - 1;
  return '.' if $count <= 0;
  return File::Spec->catfile( ('..') x $count );
}


sub render {
  my ($text) = @_;

  return markdown($text,
                  {
                   empty_element_suffix     => '>',
                   trust_list_start_value   => 1,
                   tab_width                => 4,
                  });
}


sub make_page {
  my ($template, $values) = @_;

  $template =~ s/\{(\w+)\}/$values->{$1}/ges;
  return $template;
}


# Retrieve all directives as key/value pairs and the contents of $text
# minus the directive lines.
sub split_directives {
  my ($text) = @_;

  my @lines = split(/\n/, $text);
  my @cleantext = ();

  while(@lines && $lines[0] =~ /^\s*$/) {
    push @cleantext, (shift @lines);
  }

  if ($lines[0] !~ /^;/) {
    return ({}, $text);
  }

  my %directives = ();
  while(@lines && $lines[0] =~ /^;/) {
    local $_ = shift @lines;

    next if /^;;/;
    /^;\s* (\w+) \s* = \s* (.+) $/x or die "Malformed directive: $_\n";

    my ($dir, $val) = ($1, $2);
    $val =~ s/\s*$//;

    $directives{$dir} = $val;
  }

  push @cleantext, @lines;

  return (\%directives, join("\n", @cleantext));
}


{
  my $now = time;
  my $year = 1900 + [localtime($now)]->[5];
  my $mod_date = localtime($now);

  sub generate {
    my ($srcDir, $destDir, $path, $template) = @_;

    my $srcPath = File::Spec->catdir($srcDir, $path);
    my $destPath = File::Spec->catdir($destDir, $path);
    $destPath =~ s/\.md$/.html/;

    # if (-f $destPath && -M $destPath < -M $srcPath) {
    #   msg "$srcPath unchanged; skipping.";
    #   return;
    # }

    msg "Rendering '$path'";

    my $srcText = slurp($srcPath);

    my $directives;
    ($directives, $srcText) = split_directives($srcText);

    my $root = find_rootpath($path);
    my $stylesheetUrl = File::Spec->catdir($root, "style.css");

    my $output = make_page($template,
                           {
                            title     => find_title($srcText),
                            root      => $root,
                            mod_date  => $mod_date,
                            year      => $year,
                            stylesheet=> $stylesheetUrl,
                            content   => render($srcText),
                           });


    make_path(dirname($destPath));
    unslurp($destPath, $output);
  }
}



sub main {
  my ($template, $root, $dest) = @_;

  my $tpl = slurp($template);

  my @sources;
  find(sub {
         return unless -f && /\.md$/;
         push @sources, File::Spec->abs2rel($File::Find::name, $root);
       },
       $root);

  generate($root, $dest, $_, $tpl) foreach @sources;
}


{
  my ($template, $root, $dest) = @ARGV;
  die "USAGE: $0 <template> <src-dir> <dest-dir>\n"
    unless $dest && -f $template && -d $root && !!$dest;

  main($template, $root, $dest);
}
