

all:
	( [ -d public/images ] || mkdir -p public/images )
	( [ -d public/dl ] || mkdir -p public/dl )
	perl scripts/render.pl src/page-template.html src/ public/
	cp -a assets/* public/
	(for i in images/*.png; do convert $$i public/$${i%.png}.jpg; done )
	(for f in `find releases -type f`; do cp $$f public/dl/; done )


clean:
	rm -rf public
