## Download

### Version 2.2.2

* [GitLab Release](https://gitlab.com/relarn/relarn/-/tags/release-2.2.2)

#### Source Code

* [relarn-release-2.2.2.tar.gz](https://gitlab.com/relarn/relarn/-/archive/release-2.2.2/relarn-release-2.2.2.tar.gz)
* [relarn-release-2.2.2.zip](https://gitlab.com/relarn/relarn/-/archive/release-2.2.2/relarn-release-2.2.2.zip)

(There are currently no pre-built binaries available.)


### Version 2.2.1

This is the de-facto 2.2.0 release.  (I found a bug just after typing
`git push`.)

Installation notes are [here](binary-install.html).  Previous releases
are [here](releases.html).

#### Repository

* [GitLab Release](https://gitlab.com/relarn/relarn/-/tags/release-2.2.1)
* [List of Changes](https://gitlab.com/relarn/relarn/-/blob/a4f55543cb9f74326f86c7948437b6c775cc7405/Changes.md)

#### Source Code

* [relarn-2.2.1-src.tar.gz](dl/relarn-2.2.1-src.tar.gz)
* [relarn-2.2.1-src.zip](dl/relarn-2.2.1-src.zip)

#### Retro Fonts

Here is a collection of retro-inspired fonts stolen from 
[Cool Retro Term](dl/https://github.com/Swordfish90/cool-retro-term):

* [crt-fonts.zip](dl/crt-fonts.zip)

Drop them in your `.relarn` directory, add one to your `relarnrc` file
and crank some synthwave to recreate that '80s vibe.

#### Linux Binaries

We provide both 32-bit and 64-bit binaries.

Windowed (SDL) version:

* [relarn-2.2.1-linux-i386-sdl.tar.gz](dl/relarn-2.2.1-linux-i386-sdl.tar.gz)
* [relarn-2.2.1-linux-i386-sdl.zip](dl/relarn-2.2.1-linux-i386-sdl.zip)
* [relarn-2.2.1-linux-x64-sdl.tar.gz](dl/relarn-2.2.1-linux-x64-sdl.tar.gz)
* [relarn-2.2.1-linux-x64-sdl.zip](dl/relarn-2.2.1-linux-x64-sdl.zip)

Console (TTY) version:

* [relarn-2.2.1-linux-i386.tar.gz](dl/relarn-2.2.1-linux-i386.tar.gz)
* [relarn-2.2.1-linux-i386.zip](dl/relarn-2.2.1-linux-i386.zip)
* [relarn-2.2.1-linux-x64.tar.gz](dl/relarn-2.2.1-linux-x64.tar.gz)
* [relarn-2.2.1-linux-x64.zip](dl/relarn-2.2.1-linux-x64.zip)


#### macOS Binaries

Windowed (SDL) version:

* [relarn-2.2.1-macos-sdl.tar.gz](dl/relarn-2.2.1-macos-sdl.tar.gz)
* [relarn-2.2.1-macos-sdl.zip](dl/relarn-2.2.1-macos-sdl.zip)

Console (TTY) version:

* [relarn-2.2.1-macos.tar.gz](dl/relarn-2.2.1-macos.tar.gz)
* [relarn-2.2.1-macos.zip](dl/relarn-2.2.1-macos.zip)

Windowed (SDL) version as a macOS App bundle (EXPERIMENTAL)

* [relarn-2.2.1-macos-sdl.dmg](dl/relarn-2.2.1-macos-sdl.dmg)

#### Windows Binaries

Only the windowed version is supported on Windows.

* [relarn-2.2.1-windows-sdl.tar.gz](dl/relarn-2.2.1-windows-sdl.tar.gz)
* [relarn-2.2.1-windows-sdl.zip](dl/relarn-2.2.1-windows-sdl.zip)

(Note: you may need to install SDL and SDL_ttf DLLs yourself.)


