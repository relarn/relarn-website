
## Screenshots

Here are a couple of screenshots from the new 2.2 windowed mode.  As
you can see, it looks a lot like running the game in a terminal.

Here's the town:

<center>![Town!](images/ss-2.2-sdl-town.jpg)</center>

And here's a cave level:

<center>![Cave!](images/ss-2.2-sdl-dungeon.jpg)</center>

Here are some shots from the previous version running in a
terminal.  To make it look cooler, I'm using
[Cool Retro Term](https://github.com/Swordfish90/cool-retro-term) to
bring the 80s Unix look.

This is some typical gameplay:

<center>![Caves!](images/ss-2.1-crt-dungeon.jpg)</center>

<center>![More caves!](images/ss-2.1-crt-dungeon-2.jpg)</center>

And here's the nifty new(ish) inventory menu:

<center>![Inventory menu](images/ss-2.1-crt-inventory.jpg)</center>

While CRT can display colours, it's not quite flexible enough to show
the field-of-view or unexplored squares so I had to turn those off for
these.  (I guess that's true to the whole retro look though.)

On `rxvt`, it looks fine:

<center>![Inventory menu](images/ss-2.1-rxvt-dungeon-2.jpg)</center>

And here it is on a macOS Terminal with a light background:

<center>![Inventory menu](images/ss-2.1-terminal-los.jpg)</center>

