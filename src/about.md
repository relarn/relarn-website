
## About ReLarn

ReLarn is a fork of the classic old-school Roguelike game
[Ularn](http://www.ularn.org/) (which itself was a fork of the classic
old-school Roguelike game [Larn](https://larn.org/)).  Unlike most
forks of Roguelikes, ReLarn (mostly) doesn't add weapons, monsters or
other gameplay and instead focuses on making the game work smoothly on
modern hardware and making the source code easier to read and
maintain.

That said, there *are* some gameplay changes.  Some came out of source
code cleanup, others fixed longstanding annoyances and there are a few
things I put in just for fun.  But mostly, it's still the classic
Ularn experience with less irritation.

Notable player-visible changes are:

* There's now graphical version that uses an SDL window instead of
  terminal.  (It still only shows text, though.)
* Item selection is now done via a full-screen menu.
* The game makes much more use of colour where that's supported.
* Field of view is now determined by line-of-sight instead of just
  distance and is highlighted on the screen.
* End-of-game junk mail is now no longer actually mailed to you.
* You can now select your spouse's gender and "nonbinary" is an
  available option for both of you.
* There is a new artifact, the Diploma.
* The lore is a bit more consistent ("Larn" is treated as a place name
  now, for example).  Also, more jokes.
* Generally less jankiness.
* Many small usability improvements.

Notable developer-level changes are:

* The code is now idiomatic modern C and compiles without (many)
  warnings.
* Sources have been refactored into modules and adhere (much more) to
  the DRY principle.
* Display handling now goes through just one module and uses Curses as
  its back end.
* Operating system access goes through the standard C library or
  through a thin abstraction layer.
* Anti-cheating measures have been removed; they don't work, they
  complicate the code and they make saved games more fragile.

I started this project in 2010 while working a particularly
challenging job and needed a creative(ish) project that was not overly
mentally taxing.  Refactoring a program that already (mostly) worked
turned out to be the right thing and I worked on this off and on until
2018 when I got sick of it and shipped what I had.

Since then, I've worked on it off and on just for fun.

## Resources

* [Up-to-date List of Changes](https://gitlab.com/relarn/relarn/-/blob/master/Changes.md)
* [Man page](relarn-manpage.html)

## Contacting Me

* [GitLab Project](https://gitlab.com/relarn/relarn)
* [Email](mailto:chris@blit.ca)
* [Mastodon](https://freeradical.zone/@suetanvil)


