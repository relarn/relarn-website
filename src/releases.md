## Releases

### 2.2.2 (Bug fixes; save-file compatabile)

* [GitLab Release](https://gitlab.com/relarn/relarn/-/tags/release-2.2.2)
* [List of Changes](https://gitlab.com/relarn/relarn/-/blob/release-2.2.2/Changes.md)


### 2.2.0 (Major release; saves are NOT backwards compatible)

* [GitLab Release](https://gitlab.com/relarn/relarn/-/tags/release-2.2.0)
* [List of Changes](https://gitlab.com/relarn/relarn/-/blob/29f69fc7f5a0db60b6f1324f37e365b55f51e88b/Changes.md)

**Binaries are coming soon!**

### 2.1.1 (Minor bug fix; saves ARE backwards compatible with 2.1.0)

* [GitLab Release](https://gitlab.com/relarn/relarn/-/releases/release-2.1.1)

### 2.1.0 (Next major release; saves are NOT backwards compatible)

* [GitLab Release](https://gitlab.com/relarn/relarn/-/tags/release-2.1.0)
* [List of Changes](https://gitlab.com/relarn/relarn/-/blob/ddc5a5bb2e52157973f822961486a9af8463d408/Changes.md)


### 2.0.0 (The first release!)

* [GitLab Release](https://gitlab.com/relarn/relarn/-/tags/release-2.0.0)
* [List of Changes](https://gitlab.com/relarn/relarn/-/blob/710890eb0e299ae27440d6c20f9d5aa166f8511d/Changes.md)

