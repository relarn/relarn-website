# Binary Releases

## General

Binary releases are distributed as tarballs or zipballs (sorry, no
installers or packages) containing files in the common Unix layout.
As such, you could unzip one and use `cp -a` to copy them into a
system-wide location (e.g. `/usr/local/games`).  However, you're
probably better off just unpacking them somewhere and creating a
launch script somewhere in your path.

## Linux

Note that the SDL version of ReLarn requires at least SDL 2.0.5 (plus
the corresponding SDL TTF library).  If you use Ubuntu Linux, this
means you will need to use at least Ubuntu 18LTS (or do something
unnecessarily clever).  You can install the dependencies with:

    sudo apt install libsdl2-2.0-0 libsdl2-ttf-2.0-0

The TTY version is mostly statically linked and probably doesn't need
anything else.  (I tried it on a stock Ubuntu 14 Docker image and it
Just Worked so I'm calling it done.)


## Windows

There is no installer for ReLarn.  Instead, you just unzip the files
somewhere and (optionally) create shortcuts to the programs in `bin`.
To uninstall, just delete the folder.

ReLarn stores most of your game-related files in the folder `ReLarn`
in `Documents`.  If you have moved it to a non-standard location, it
may have trouble finding it.  It will also have trouble if there are
non-ASCII Unicode characters in your path; ReLarn has trouble dealing
with Unicode on Windows.  In any of these cases (or if you just want
to move the folder somewhere else), you can override the location by
setting the environment variable `RELARN_CONFIG_HOME` to your prefered
folder.


## macOS

The preferred way to install the macOS binaries is to do the Linux
thing and unpack the tarball or zipball somewhere and then optionally
either add the `bin` directory to your path or create a launch
script.  You should not need to install anything else for ReLarn to
work.

There is also a `dmg` file containing a macOS app bundle.  This was
made without the conventional macOS toolchain and so may not work for
you.  If not, um, oops?

The binaries were built on my development machine which runs Mojave
and were briefly tested against on a computer running High Sierra.  I
have *not* tested it against Catalina.
