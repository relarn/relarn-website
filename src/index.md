
ReLarn is an old-school text-based Roguelike game based on
[Ularn](http://www.ularn.org/) but with many quality-of-life
improvements and much better support for modern computers and
operating systems.

## News

### 2023-10-03 - Release 2.2.2

This release fixes a few gameplay and non-gameplay bugs, includes a default font
for the SDL version to use, and adds a few internal and build-process
tweaks.  See [Changes.md](https://gitlab.com/relarn/relarn/-/blob/release-2.2.2/Changes.md)
for details.


### 2020-07-01 - Binaries for 2.2.1

There are now binaries available for Windows, Linux and macOS
[here](download.html) with installation notes
[here](binary-install.html).

There are also [new screenshots](screenshots.html).

### 2020-06-29 - New Release, 2.2.0^H1

Version 2.2.0 is finally out.

This version adds:
    
* Support for native (non-Cygwin) Microsoft Windows
* Can now use OS windows instead of a terminal (via SDL and PDCurses)
* Colouring is now much more configurable
* Many more tweaks, bug fixes and code quality improvements.

More details [here](https://gitlab.com/relarn/relarn/-/blob/29f69fc7f5a0db60b6f1324f37e365b55f51e88b/Changes.md).

Check it out in the [downloads page](download.html).  Binaries are
coming soon.

#### Update

And as soon as I pushed everything to the outside world, I found a
tiny bug in parsing the config file.

It figures.

It's now fixed.  The latest release is now 2.2.1.  Binaries are still
on the way.


### 2020-06-02 - Website Update and move to Gitlab

The project is now hosted on Gitlab at
<https://gitlab.com/relarn/relarn>.  The
[GitHub repo](https://github.com/relarn/relarn) will continue to be a
mirror of this repository for the forseeable future although some
related repositories (this website, the CI build scripts) may be
removed from there.

In addition, the website has been redesigned to really lean in to the
80s Unix aesthetic.

