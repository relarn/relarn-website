## Related Websites

* [Wikipedia on (U)Larn](https://en.wikipedia.org/wiki/Larn_(video_game)).
* [Current](http://www.ularn.org/) Ularn home page and [Github repo](https://github.com/joshbressers/ularn).
* Phil Cordier's [original](https://web.archive.org/web/20050205235118/http://www.cordier.com:80/ularn/index.html) (archived) Ularn home page.
* [iLarn](http://roguelike-palm.sourceforge.net/iLarn/index.php), ULarn for PalmOS.
* [Larn](http://larn.org/) in your browser, plus some more history.
* kmh's spoiler file ([cleaned up local copy](kmh_ularn.html) and [the original, plain text](http://kmh678.tripod.com/Computers/ularn.text)).
